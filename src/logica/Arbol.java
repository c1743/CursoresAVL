package logica;

import java.util.ArrayList;
import java.util.Stack;

/*
//@author: AndresFWilT
//
 */
public class Arbol {

    //Declaracion de variables
    int raiz;
    private String titulo;
    String existente;
    ArrayList<String> palabras = new ArrayList<String>();
    ArrayList<String> raicesRepetidas = new ArrayList<String>();
    private int proxDisp;
    private int filas = 11;
    private String[][] cursor;

    //Constructor
    public Arbol(String titulo) {
        raiz = 0;
        this.titulo = titulo;
        cursor = new String[filas][5];
        for (int i = 0; i < cursor.length; i++) {
            for (int j = 0; j < 5; j++) {
                cursor[i][0] = i + "";
                cursor[i][1] = "";
                cursor[i][3] = "0";
                cursor[i][2] = "0";
                cursor[i][4] = "0";
            }
        }
        cursor[10][3] = "0";
    }

    //Metodos
    public int insertarNodo(String letra, int nuevo) {
        setPalabra(letra);
        int p, q, s, pivote, pp, altura;
        pp = q = 0;
        if (raiz == 0) {
            raiz = nuevo;
            cursor[1][1] = letra;
            return 0;
        }
        pivote = p = raiz;
        cursor[nuevo][1] = letra;
        while (!"".equals(cursor[p][1])) {
            if (!"0".equals(cursor[p][4])) {
                pp = q;
                pivote = p;
            }

            if (cursor[p][1].compareTo(letra) == 0) {
                existente = cursor[p][1];
                return 2;
            } else {
                q = p;
                if (cursor[p][1].compareTo(letra) > 0) {
                    p = Integer.valueOf(cursor[p][2]);
                } else {
                    p = Integer.valueOf(cursor[p][3]);
                }
            }
        }
        if (cursor[q][1].compareTo(letra) > 0) {
            cursor[q][2] = String.valueOf(nuevo);
        } else {
            cursor[q][3] = String.valueOf(nuevo);
        }

        if (cursor[pivote][1].compareTo(letra) > 0) {
            s = Integer.valueOf(cursor[pivote][2]);
            altura = 1;
        } else {
            s = Integer.valueOf(cursor[pivote][3]);
            altura = -1;
        }

        p = s;
        while (p != nuevo) {

            if (cursor[p][1].compareTo(letra) > 0) {
                cursor[p][4] = "1";
                p = Integer.valueOf(cursor[p][2]);
            } else {
                cursor[p][4] = "-1";
                p = Integer.valueOf(cursor[p][3]);
            }
        }
        if (cursor[pivote][4].equals("0")) {
            cursor[pivote][4] = String.valueOf(altura);
        } else if (Integer.valueOf(cursor[pivote][4]) + altura == 0) {
            cursor[pivote][4] = "0";
        } else {
            if (altura == 1) {
                if (cursor[s][4].equals("1")) {
                    rotacionDer(pivote, s);
                } else {
                    s = rotacionDobleDer(pivote, s);
                }
            } else {
                if (cursor[s][4].equals("-1")) {
                    rotacionIzq(pivote, s);
                } else {
                    s = rotacionDobleIzq(pivote, s);
                }
            }
            if (pp == 0) {
                raiz = s;
            } else if (Integer.valueOf(cursor[pp][2]) == pivote) {
                cursor[pp][2] = String.valueOf(s);
            } else {
                cursor[pp][3] = String.valueOf(s);
            }
        }
        return 1;
    }

    public int retirarDato(String palabra) {
        borrarPalabra(palabra);
        int n = valorPalabra(palabra);
        Stack pila = new Stack();
        int p, q, t, r;
        int llave, accion;
        // Para trabajar terminar por re
        int[] terminar = new int[1];
        boolean encontro = false;
        if (raiz == 0) {
            return 1;
        }
        terminar[0] = 0;
        p = raiz;

        while (!encontro && p != 0) {

            pila.push(p);
            if (n < valorPalabra(cursor[p][1])) {
                p = Integer.valueOf(cursor[p][2]);
            } else if (n > valorPalabra(cursor[p][1])) {
                p = Integer.valueOf(cursor[p][3]);
            } else {
                existente = String.valueOf(p);
                encontro = true;
            }
        }

        if (!encontro) {
            return 2;
        }
        t = 0;
        p = (int) pila.pop();
        llave = valorPalabra(cursor[p][1]);
        if ("0".equals(cursor[p][2]) && "0".equals(cursor[p][3])) {
            accion = 0;
        } else if ("0".equals(cursor[p][3])) {
            accion = 1;
        } else if ("0".equals(cursor[p][2])) {
            accion = 2;
        } else {
            accion = 3;
        }

        if (accion == 0 || accion == 1 || accion == 2) {
            if (!pila.empty()) {
                q = (int) pila.pop();
                if (llave < valorPalabra(cursor[q][1])) {
                    switch (accion) {
                        case 0:
                        case 1:
                            cursor[q][2] = cursor[p][2];
                            t = balanceDer(q, terminar);
                            break;
                        case 2:
                            cursor[q][2] = cursor[p][3];
                            t = balanceDer(q, terminar);
                            break;
                    }
                } else {
                    switch (accion) {
                        case 0:
                        case 2:
                            cursor[q][3] = cursor[p][3];
                            t = balanceIzq(q, terminar);
                            break;
                        case 1:
                            cursor[q][3] = cursor[p][2];
                            t = balanceIzq(q, terminar);
                            break;
                    }
                }
            } else {
                switch (accion) {
                    case 0:
                        raiz = 0;
                        terminar[0] = 1;
                        break;
                    case 1:
                        raiz = Integer.valueOf(cursor[p][2]);
                        break;
                    case 2:
                        raiz = Integer.valueOf(cursor[p][3]);
                        break;
                }
            }

        } else {
            pila.push(p);
            r = p;
            p = Integer.valueOf(cursor[r][3]);
            q = 0;

            while (!"0".equals(cursor[p][2])) {
                pila.push(p);
                q = p;
                p = Integer.valueOf(cursor[p][2]);
            }
            cursor[r][1] = cursor[p][1];
            llave = valorPalabra(cursor[r][1]);
            if (q != 0) {
                cursor[q][2] = cursor[p][3];
                t = balanceDer(q, terminar);
            } else {
                cursor[r][3] = cursor[p][3];
                t = balanceIzq(r, terminar);
            }
            q = (int) pila.pop();
        }
        while (!pila.empty() && terminar[0] == 0) {
            q = (int) pila.pop();
            if (llave < valorPalabra(cursor[q][1])) {
                if (t != 0) {
                    cursor[q][2] = String.valueOf(t);
                    t = 0;
                }
                t = balanceDer(q, terminar);
            } else {
                if (t != 0) {
                    cursor[q][3] = String.valueOf(t);
                    t = 0;
                }
                t = balanceIzq(q, terminar);
            }
        }
        if (t != 0) {
            if (pila.empty() == true) {
                raiz = t;
            } else {
                q = (int) pila.pop();
                if (llave < valorPalabra(cursor[q][1])) {
                    cursor[q][2] = String.valueOf(t);
                } else {
                    cursor[q][3] = String.valueOf(t);
                }
            }
        }
        cursor[p][1] = "";
        cursor[p][2] = "0";
        cursor[p][3] = "0";
        cursor[p][4] = "0";
        return p;
    }

    void rotacionDer(int p, int q) {
        cursor[p][4] = "0";
        cursor[q][4] = "0";
        cursor[p][2] = cursor[q][3];
        cursor[q][3] = String.valueOf(p);
    }

    void rotacionIzq(int p, int q) {
        cursor[p][4] = "0";
        cursor[q][4] = "0";
        cursor[p][3] = cursor[q][2];
        cursor[q][2] = String.valueOf(p);
    }

    int rotacionDobleDer(int p, int q) {
        int r;
        r = Integer.valueOf(cursor[q][3]);
        cursor[q][3] = cursor[r][2];
        cursor[r][2] = String.valueOf(q);
        cursor[p][2] = cursor[r][3];
        cursor[r][3] = String.valueOf(p);

        switch (cursor[r][4]) {
            case "-1":
                cursor[q][4] = "1";
                cursor[p][4] = "0";
                break;
            case "0":
                cursor[q][4] = cursor[p][4] = "0";
                break;
            case "1":
                cursor[q][4] = "0";
                cursor[p][4] = "-1";
                break;
        }
        cursor[r][4] = "0";
        return r;
    }

    int rotacionDobleIzq(int p, int q) {
        int r;
        r = Integer.valueOf(cursor[q][2]);
        cursor[q][2] = cursor[r][3];
        cursor[r][3] = String.valueOf(q);
        cursor[p][3] = cursor[r][2];
        cursor[r][2] = String.valueOf(p);
        switch (cursor[r][4]) {
            case "-1":
                cursor[q][4] = "0";
                cursor[p][4] = "1";
                break;
            case "1":
                cursor[q][4] = "-1";
                cursor[p][4] = "0";
                break;
            case "0":
                cursor[q][4] = cursor[p][4] = "0";
                break;
        }
        cursor[r][4] = "0";
        return r;
    }

    int balanceDer(int q, int[] terminar) {
        int t = 0;
        switch (cursor[q][4]) {
            case "1":
                cursor[q][4] = "0";
                break;
            case "-1":
                t = Integer.valueOf(cursor[q][3]);
                switch (cursor[t][4]) {
                    case "1":
                        t = rotacionDobleIzq(q, t);
                        break;
                    case "-1":
                        rotacionIzq(q, t);
                        break;
                    case "0":
                        cursor[q][3] = cursor[t][2];
                        cursor[t][2] = String.valueOf(q);
                        cursor[t][4] = "1";
                        terminar[0] = 1;
                        break;
                }
                break;
            case "0":
                cursor[q][4] = "-1";
                terminar[0] = 1;
                break;
        }
        return t;
    }

    int balanceIzq(int q, int[] terminar) {
        int t = 0;
        switch (cursor[q][4]) {
            case "-1":
                cursor[q][4] = "0";
                break;
            case "1":
                t = Integer.valueOf(cursor[q][2]);
                switch (cursor[t][4]) {
                    case "1":
                        rotacionDer(q, t);
                        break;
                    case "-1":
                        t = rotacionDobleDer(q, t);
                        break;
                    case "0":
                        cursor[q][2] = cursor[t][3];
                        cursor[t][3] = String.valueOf(q);
                        cursor[t][4] = "-1";
                        terminar[0] = 1;
                        break;
                }
                break;
            case "0":
                cursor[q][4] = "1";
                terminar[0] = 1;
                break;
        }
        return t;
    }

    private int valorPalabra(String palabra) {
        String[] letras = palabra.split("");
        int total = 0;
        for (String letra : letras) {
            int pos = 1;
            for (char j = 'a'; j < 'z'; j++) {
                if (letra.charAt(0) == j) {
                    total += pos;
                }
                pos++;
            }
        }
        return total;
    }

    public String[][] datosF(String[][] aux, int p) {
        aux = llenarDisp(aux);
        proxDisp = proximoDisponible(aux);
        setProxDisp(proxDisp);
        return aux;
    }

    private int siguienteDisp() {
        int i = 0;
        for (i = 1; i < cursor.length; i++) {
            if (cursor[i][1].equals("")) {
                return i;
            }
        }
        return i;
    }

    private String[][] llenarDisp(String[][] aux) {
        boolean primero = true, siguiente;
        int pos1 = 0;
        for (int i = 1; i < aux.length - 1; i++) {
            siguiente = true;
            if ("".equals(aux[i][1])) {
                if (primero) {
                    pos1 = i;
                }
                primero = false;
                for (int j = i + 1; j < aux.length; j++) {
                    if ("".equals(aux[j][1]) && siguiente) {
                        aux[i][3] = String.valueOf(j);
                        siguiente = false;
                    }

                }
            }

        }
        return aux;
    }

    private int proximoDisponible(String[][] aux) {
        int i = 1;
        for (i = 1; i < aux.length; i++) {
            if (aux[i][1].equals("")) {
                return i;
            }
        }
        return i;
    }

    public void aumentarTamaño() {
        filas = filas + 13;
        System.out.println(filas);
        String aux[][] = new String[filas][5];
        for (int i = 0; i < aux.length; i++) {
            for (int j = 0; j < 5; j++) {
                if (i < cursor.length) {
                    aux[i][j] = cursor[i][j];
                } else {
                    aux[i][0] = i + "";
                    aux[i][1] = "";
                    aux[i][3] = "0";
                    aux[i][2] = "0";
                    aux[i][4] = "0";
                }
            }
        }
        cursor = new String[filas][5];
        cursor = aux;
    }

    public void imprimirMatriz() {
        System.out.println("Impresion: ");
        for (int j = 0; j < cursor.length; j++) {
            for (int k = 0; k < cursor[0].length; k++) {
                System.out.print(cursor[j][k] + " \t");
            }
            System.out.println("");
        }
    }

    //Getters y Setters
    /**
     * @return the matriz
     */
    public String[][] getMatriz() {
        cursor[0][3] = String.valueOf(siguienteDisp());
        cursor[0][2] = String.valueOf(raiz);
        return cursor;
    }

    private void setPalabra(String letra) {
        palabras.add(letra);
    }
    
    public int getNumeroPalabras(){
        return palabras.size();
    }

    public boolean getPalabra(String palabra) {
        for (int i = 0; i < palabras.size(); i++) {
            if (palabra.equals(palabras.get(i))) {
                return true;
            }
        }
        return false;
    }

    private void borrarPalabra(String palabra) {
        for (int i = 0; i < palabras.size(); i++) {
            if (palabra.equals(palabras.get(i))) {
                palabras.remove(i);
            }
        }
    }

    private void setProxDisp(int proxDisp) {
        this.proxDisp = proxDisp;
    }

    public int getProxDisp() {
        return proxDisp;
    }
}
