/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import logica.Arbol;

/**
 *
 * @author AndresFWilT
 */
public class ventana extends javax.swing.JFrame {

    //Declaracion de objetos
    Arbol a;
    //Declaracion de variables globales
    String nCursor;
    String palabra;
    String[][] matrizDatos;
    String[][] matriz;
    int Filas = 11;
    int nuevo = 1;
    //Declaracion de modelos
    DefaultTableModel modelo;

    public ventana() {
        this.setTitle("Cursores");
        this.setResizable(false);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton3 = new javax.swing.JButton();
        lCursor = new javax.swing.JLabel();
        tCursor = new javax.swing.JTextField();
        bCursor = new javax.swing.JButton();
        lNombre = new javax.swing.JLabel();
        tPalabra = new javax.swing.JTextField();
        bInsertar = new javax.swing.JButton();
        bEliminar = new javax.swing.JButton();
        bListar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabla = new javax.swing.JTable();
        lRaiz = new javax.swing.JLabel();
        lProx = new javax.swing.JLabel();

        jButton3.setText("jButton3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lCursor.setText("Nombre del cursor");

        tCursor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tCursorKeyTyped(evt);
            }
        });

        bCursor.setText("Inicializar");
        bCursor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCursorActionPerformed(evt);
            }
        });

        lNombre.setText("Digite nombre a insertar o eliminar");
        lNombre.setVisible(false);

        tPalabra.setVisible(false);

        bInsertar.setText("Insertar");
        bInsertar.setVisible(false);
        bInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bInsertarActionPerformed(evt);
            }
        });

        bEliminar.setText("Eliminar");
        bEliminar.setVisible(false);
        bEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEliminarActionPerformed(evt);
            }
        });

        bListar.setText("Listar");
        bListar.setVisible(false);
        bListar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bListarActionPerformed(evt);
            }
        });

        Tabla.setVisible(false);
        Tabla.setVisible(false);
        jScrollPane1.setViewportView(Tabla);

        lRaiz.setText("Raiz:");
        lRaiz.setVisible(false);

        lProx.setText("Proximo disponible:");
        lProx.setVisible(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bCursor, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(bInsertar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bEliminar)
                                .addGap(5, 5, 5)
                                .addComponent(bListar))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lRaiz, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lNombre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lCursor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tCursor)
                            .addComponent(tPalabra)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lProx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tCursor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lCursor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bCursor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lNombre)
                    .addComponent(tPalabra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bInsertar)
                    .addComponent(bEliminar)
                    .addComponent(bListar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lRaiz)
                    .addComponent(lProx))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    //Boton cursor
    private void bCursorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCursorActionPerformed
        if (tCursor.getText().equals(null) | tCursor.getText().equals("")) {
            JOptionPane.showMessageDialog(getContentPane(), "Ingrese el nombre del cursor", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (tCursor.getText().length() > 4) {
            JOptionPane.showMessageDialog(getContentPane(), "La longitud del cursor debe ser menor a 4", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            nCursor = tCursor.getText();
            inicializaMatriz();
            a = new Arbol(nCursor);
            JOptionPane.showMessageDialog(getContentPane(), "Cursor '" + nCursor + "' inicializado", "¡Atencion!", JOptionPane.INFORMATION_MESSAGE);
            bInsertar.setVisible(true);
            bListar.setVisible(true);
            bEliminar.setVisible(true);
            lNombre.setVisible(true);
            tPalabra.setVisible(true);
            tCursor.setText("");
        }
    }//GEN-LAST:event_bCursorActionPerformed
    //Textfield cursor
    private void tCursorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tCursorKeyTyped
        if (tCursor.getText().length() == 4) {
            evt.consume();
        }
    }//GEN-LAST:event_tCursorKeyTyped
    //Boton listar
    private void bListarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bListarActionPerformed
        crearModelo();
        mostrarHeader();
        ponerDatos();
        Tabla.setVisible(true);
        lProx.setVisible(true);
        lRaiz.setVisible(true);
    }//GEN-LAST:event_bListarActionPerformed
    //Boton insertar
    private void bInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bInsertarActionPerformed
        if (tPalabra.getText().equals(null) | tPalabra.getText().equals("")) {
            JOptionPane.showMessageDialog(getContentPane(), "Ingrese la palabra", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (a.getPalabra(tPalabra.getText())) {
            JOptionPane.showMessageDialog(getContentPane(), "Palabra repetida", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            if (nuevo >= 10 && nuevo % 10 == 0) {
                Filas = Filas + 13;
                inicializaMatriz();
                crearModelo();
                mostrarHeader();
                ponerDatos();
                a.aumentarTamaño();
            }
            palabra = tPalabra.getText();
            a.insertarNodo(palabra, nuevo);
            JOptionPane.showMessageDialog(getContentPane(), "Palabra '" + palabra + "' insertada", "¡Atencion!", JOptionPane.INFORMATION_MESSAGE);
            tPalabra.setText("");
            for (int i = 0; i < a.getMatriz().length; i++) {
                for (int j = 0; j < 5; j++) {
                    matrizDatos[i][j] = a.getMatriz()[i][j];
                }
            }
            matrizDatos = a.datosF(matrizDatos, nuevo);
            Tabla.setVisible(false);
            lProx.setVisible(false);
            lRaiz.setVisible(false);
            nuevo = proximoDisponible(matrizDatos);
        }
    }//GEN-LAST:event_bInsertarActionPerformed
    //Boton eliminar
    private void bEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEliminarActionPerformed
        if (tPalabra.getText().equals(null) | tPalabra.getText().equals("")) {
            JOptionPane.showMessageDialog(getContentPane(), "Ingrese la palabra", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (!a.getPalabra(tPalabra.getText())) {
            JOptionPane.showMessageDialog(getContentPane(), "La palabra no existe", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            palabra = tPalabra.getText();
            int p = a.retirarDato(palabra);
            JOptionPane.showMessageDialog(getContentPane(), "Palabra '" + palabra + "' eliminada", "¡Atencion!", JOptionPane.INFORMATION_MESSAGE);
            tPalabra.setText("");
            for (int i = 0; i < a.getMatriz().length; i++) {
                for (int j = 0; j < 5; j++) {
                    matrizDatos[i][j] = a.getMatriz()[i][j];
                }
            }
            matrizDatos = a.datosF(matrizDatos, p);
            nuevo = proximoDisponible(matrizDatos);
            Tabla.setVisible(false);
            lProx.setVisible(false);
            lRaiz.setVisible(false);
        }
    }//GEN-LAST:event_bEliminarActionPerformed
    //Creacion de la tabla
    private void crearModelo() {
        DefaultTableModel modelo = (DefaultTableModel) Tabla.getModel();
        modelo.setRowCount(Filas);
        modelo.setColumnCount(5);
        for (int i = 0; i < Filas; i++) {
            for (int j = 0; j < 5; j++) {
                Tabla.setModel(modelo);
            }
        }
        Tabla.setDefaultEditor(Object.class, null);
    }

    private void mostrarHeader() {
        JTableHeader head = Tabla.getTableHeader();
        TableColumnModel cModelo = head.getColumnModel();
        TableColumn columna = null;
        for (int i = 0; i < 5; i++) {
            columna = cModelo.getColumn(i);
            if (i == 0) {
                columna.setHeaderValue("Registro");
            } else if (i == 1) {
                columna.setHeaderValue("Nombre");
            } else if (i == 2) {
                columna.setHeaderValue("Izquierda");
            } else if (i == 3) {
                columna.setHeaderValue("Derecha");
            } else if (i == 4) {
                columna.setHeaderValue("Balance");
            }
        }
        Tabla.repaint();
    }

    private void inicializaMatriz() {
        matrizDatos = new String[Filas][5];
    }

    //Completar tabla matriz
    private void ponerDatos() {
        for (int i = 0; i < Filas; i++) {
            for (int j = 0; j < 5; j++) {
                Tabla.setValueAt(matrizDatos[i][j], i, j);
            }
        }
        lRaiz.setText("Raiz: " + matrizDatos[0][2]);
        lProx.setText("Proximo disponible: " + matrizDatos[0][3]);
    }

    private int proximoDisponible(String[][] aux) {
        int i = 1;
        for (i = 1; i < aux.length; i++) {
            if (aux[i][1].equals("")) {
                return i;
            }
        }
        return i;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Tabla;
    private javax.swing.JButton bCursor;
    private javax.swing.JButton bEliminar;
    private javax.swing.JButton bInsertar;
    private javax.swing.JButton bListar;
    private javax.swing.JButton jButton3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lCursor;
    private javax.swing.JLabel lNombre;
    private javax.swing.JLabel lProx;
    private javax.swing.JLabel lRaiz;
    private javax.swing.JTextField tCursor;
    private javax.swing.JTextField tPalabra;
    // End of variables declaration//GEN-END:variables

}
